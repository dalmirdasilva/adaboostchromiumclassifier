(function() {
  
  var body = document.body;
  
  function DistillablePageDetector(adaboostProto) {
    this.adaboostProto = adaboostProto;
    this.threshold = 0;
    for (var i = 0; i < adaboostProto.numStumps; i++) {
      var stump = adaboostProto.stumps[i];
      this.threshold += stump.weight / 2.0;
    }
  }

  DistillablePageDetector.prototype.classify = function(features) {
    return this.score(features) > this.threshold;
  };

  DistillablePageDetector.prototype.score = function(features) {
    if (features.length != this.adaboostProto.numFeatures) {
      return 0.0
    }
    var score = 0;
    for (i = 0; i < this.adaboostProto.numStumps; i++) {
      var stump = this.adaboostProto.stumps[i];
      if (features[stump.featureNumber] > stump.split) {
        score += stump.weight;
      }
    }
    console.log('score: ', score)
    console.log('threshold: ', this.threshold)
    return score;
  };

  function hasOGArticle() {
    var elems = document.head.querySelectorAll('meta[property="og:type"],meta[name="og:type"]');
    for (var i in elems) {
      if (elems[i].content && elems[i].content.toUpperCase() == 'ARTICLE') {
        return true;
      }
    }
    return false;
  }

  var parseURL = function(url) {
    var parser = document.createElement('a');
    parser.href = url;
    return parser
  };

  var countFrequency = function(re, str) {
    return str.split(re).length - 1;
  };

  var calculateDerivedFeatures = function(features) {
    var derivedFeatures = [];
    var pagePath = features.url.pathname;
    var innerTextWords = countFrequency(/\w+/, features.innerText);
    var textContentWords = countFrequency(/\w+/, features.textContent);
    var innerHTMLWords = countFrequency(/\w+/, features.innerHTML);
    derivedFeatures.push(features.opengraph * 1.0);
    derivedFeatures.push(pagePath.indexOf('forum') >= 0 ? 1.0 : 0.0);
    derivedFeatures.push(pagePath.indexOf('index') >= 0 ? 1.0 : 0.0);
    derivedFeatures.push(pagePath.indexOf('view') >= 0 ? 1.0 : 0.0);
    derivedFeatures.push(pagePath.indexOf('.asp') >= 0 ? 1.0 : 0.0);
    derivedFeatures.push(pagePath.indexOf('phpbb') >= 0 ? 1.0 : 0.0);
    derivedFeatures.push(pagePath.indexOf('.php') >= 0 ? 1.0 : 0.0);
    derivedFeatures.push(pagePath.length * 1.0);
    derivedFeatures.push(pagePath.length < 2 ? 1.0 : 0.0);
    derivedFeatures.push(countFrequency(/\/./, pagePath) * 1.0);
    derivedFeatures.push(countFrequency(/[^\w/]/, pagePath) * 1.0);
    derivedFeatures.push(countFrequency(/\d+/, pagePath) * 1.0);
    var segments = pagePath.split('/');
    derivedFeatures.push(segments[segments.length - 1].length * 1.0);
    derivedFeatures.push(features.numForms * 1.0);
    derivedFeatures.push(features.numAnchors * 1.0);
    derivedFeatures.push(features.numElements * 1.0);
    derivedFeatures.push(features.numAnchors / Math.max(1.0, features.numElements * 1.0));
    derivedFeatures.push(features.innerText.length  * 1.0);
    derivedFeatures.push(features.textContent.length  * 1.0);
    derivedFeatures.push(features.innerHTML.length  * 1.0);
    derivedFeatures.push(features.innerText.length / Math.max(1.0, features.innerHTML.length  * 1.0));
    derivedFeatures.push(features.textContent.length / Math.max(1.0, features.innerHTML.length  * 1.0));
    derivedFeatures.push(features.innerText.length / Math.max(1.0, features.textContent.length  * 1.0));
    derivedFeatures.push(innerTextWords * 1.0);
    derivedFeatures.push(textContentWords * 1.0);
    derivedFeatures.push(innerHTMLWords * 1.0);
    derivedFeatures.push(innerTextWords / Math.max(1.0, innerHTMLWords * 1.0));
    derivedFeatures.push(textContentWords / Math.max(1.0, innerHTMLWords * 1.0));
    derivedFeatures.push(innerTextWords / Math.max(1.0, textContentWords * 1.0));
    return derivedFeatures;
  };

  var features = {
    opengraph: hasOGArticle(),
    url: parseURL(document.location.href),
    numElements: body.querySelectorAll('*').length,
    numAnchors: body.querySelectorAll('a').length,
    numForms: body.querySelectorAll('form').length,
    innerText: body.innerText,
    textContent: body.textContent,
    innerHTML: body.innerHTML
  };

  var derivedFeatures = calculateDerivedFeatures(features);
  var dec = new DistillablePageDetector(adaboostProto);
  return dec.classify(derivedFeatures);
})();

package main

import (
  "bitbucket.org/dalmirdasilva/AdaBoostChromiumClassifier/dom_distiller"
  "log"
  "fmt"
  "bytes"
)

type DistillablePageDetector struct {
  adaboostProto *dom_distiller.AdaBoostProto
  threshold     float64
}

func NewDistillablePageDetector(adaboostProto *dom_distiller.AdaBoostProto) *DistillablePageDetector {
  detector := &DistillablePageDetector{adaboostProto, 0.0}
  for i := int32(0); i < adaboostProto.GetNumStumps(); i++ {
    stump := adaboostProto.GetStump()[i]
    detector.threshold += stump.GetWeight() / 2.0
  }
  return detector
}

func (d *DistillablePageDetector) Classify(features []float64) bool {
  return d.score(features) > d.threshold;
}

func (d *DistillablePageDetector) score(features []float64) (score float64) {
  if int32(len(features)) != d.adaboostProto.GetNumFeatures() {
    log.Println("len(features) (", len(features), ") != d.adaboostProto.GetNumFeatures() (", d.adaboostProto.GetNumFeatures(), ")")
    return 0.0
  }
  for i := int32(0); i < d.adaboostProto.GetNumStumps(); i++ {
    stump := d.adaboostProto.GetStump()[i]
    if features[stump.GetFeatureNumber()] > stump.GetSplit() {
      score += stump.GetWeight()
    }
  }
  log.Println("score:", score)
  log.Println("d.threshold:", d.threshold)
  return score;
}

func (d *DistillablePageDetector) Json() (json string) {
  buffer := &bytes.Buffer{}
  buffer.WriteString("{\n")
  buffer.WriteString(fmt.Sprintf("  numStumps: %d,\n", d.adaboostProto.GetNumStumps()))
  buffer.WriteString(fmt.Sprintf("  numFeatures: %d,\n", d.adaboostProto.GetNumFeatures()))
  buffer.WriteString("  stumps: [\n    ")
  for i := int32(0); i < d.adaboostProto.GetNumStumps(); i++ {
    stump := d.adaboostProto.GetStump()[i]
    buffer.WriteString("{\n")
    buffer.WriteString(fmt.Sprintf("      featureNumber: %d, \n", stump.GetFeatureNumber()))
    buffer.WriteString(fmt.Sprintf("      split: %f, \n", stump.GetSplit()))
    buffer.WriteString(fmt.Sprintf("      weight: %f, \n", stump.GetWeight()))
    buffer.WriteString("    }, ")
  }
  buffer.WriteString("\n  ]\n")
  buffer.WriteString("}\n")
  return buffer.String()
}

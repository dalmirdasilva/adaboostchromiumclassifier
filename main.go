package main

import (
  "fmt"
  "github.com/golang/protobuf/proto"
  "io/ioutil"
  "bitbucket.org/dalmirdasilva/AdaBoostChromiumClassifier/dom_distiller"
  "net/http"
  "log"
  "encoding/json"
  "io"
)

var detector *DistillablePageDetector

func classifyHandler(w http.ResponseWriter, r *http.Request) {
  if len(r.URL.Query()["features"]) <= 0 {
    w.WriteHeader(http.StatusBadRequest)
    io.WriteString(w, "The param features should be present.")
    return
  }
  featuresParam := r.URL.Query()["features"][0]
  var features []float64
  err := json.Unmarshal([]byte(featuresParam), &features)
  if err != nil {
    w.WriteHeader(http.StatusBadRequest)
    io.WriteString(w, "Cannot unmarshal the features.")
    return
  }
  if detector.Classify(features) {
    fmt.Fprintf(w, "yes")
  } else {
    fmt.Fprintf(w, "no")
  }
}

func modelHandler(w http.ResponseWriter, r *http.Request) {
  fmt.Fprintf(w, detector.Json())
}

func main() {
  buf, err := ioutil.ReadFile("distillable_page_model.bin")
  if err != nil {
    log.Println("Cannot read distillable_page_model.bin. Error:", err)
    return
  }
  adaboost := &dom_distiller.AdaBoostProto{}
  proto.Unmarshal(buf, adaboost)
  detector = NewDistillablePageDetector(adaboost);
  http.HandleFunc("/classify", classifyHandler)
  http.HandleFunc("/model", modelHandler)
  log.Println("Listening...")
  http.ListenAndServe(":9000", nil)
}
